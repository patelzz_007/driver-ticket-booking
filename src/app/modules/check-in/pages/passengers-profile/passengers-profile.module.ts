import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PassengersProfilePageRoutingModule } from './passengers-profile-routing.module';

import { PassengersProfilePage } from './passengers-profile.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PassengersProfilePageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [PassengersProfilePage]
})
export class PassengersProfilePageModule {}
