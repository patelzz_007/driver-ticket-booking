import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassengersProfilePage } from './passengers-profile.page';

const routes: Routes = [
  {
    path: '',
    component: PassengersProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PassengersProfilePageRoutingModule {}
