import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PassengersProfilePage } from './passengers-profile.page';

describe('PassengersProfilePage', () => {
  let component: PassengersProfilePage;
  let fixture: ComponentFixture<PassengersProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassengersProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PassengersProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
