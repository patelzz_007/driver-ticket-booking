import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckInPage } from './check-in.page';

const routes: Routes = [
  {
    path: '',
    component: CheckInPage,
    children : [
      {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'trip-details',
        loadChildren: () => import('./pages/trip-details/trip-details.module').then( m => m.TripDetailsPageModule)
      },
      {
        path: 'passengers-profile',
        loadChildren: () => import('./pages/passengers-profile/passengers-profile.module').then( m => m.PassengersProfilePageModule)
      }
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckInPageRoutingModule {}
