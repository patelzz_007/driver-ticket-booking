import { Component, OnInit } from '@angular/core';
import { WidgetService } from 'src/app/services/common';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  passwordtype: string = 'password';
  cpasswordtype: string = 'password';
  password: boolean = false;
  cpassword: string;

  constructor(
    private widgets: WidgetService,
    private navCtrl: NavController
  ) {
  }

  ngOnInit() {

  }


  back() {
    this.navCtrl.pop();
  }

  async shownpassword(passwordtype) {
    if (this[passwordtype] == 'text') {
      this[passwordtype] = 'password';
      return;
    }

    this[passwordtype] = 'text';
  }

  async register() {
    const loading = await this.widgets.loading(`Creating your account...`);
    // const loading = await this.widgets.loading(`Creating ${user.email} account...`);
    try {
      // if (this.cpassword != user.password) throw "Password and confirm password did not match";


      loading.present();
      // await this.fauth.register(user);
      loading.dismiss();
      this.widgets.showSuccessModal('customer/account');

    } catch (e) {
      loading.dismiss();
      this.widgets.presentToast(e, 'danger', 5000);
    }

  }

}
