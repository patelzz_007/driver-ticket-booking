import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WidgetService } from 'src/app/services/common';
import { Plugins, CameraResultType, CameraSource, Geolocation, Capacitor, FilesystemDirectory } from '@capacitor/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Platform, LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NativeFunction } from 'src/app/services/native';

const { Camera, Filesystem } = Plugins;
@Component({
  selector: 'app-sos',
  templateUrl: './sos.page.html',
  styleUrls: ['./sos.page.scss'],
})
export class SosPage implements OnInit {

  @ViewChild('filePickerRef', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  latitude: number;
  longitude: number;
  isDesktop: boolean = false;
  photo: SafeResourceUrl;

  //Form Control
  myform: FormGroup;
  keterangan: FormControl;
  lat: FormControl;
  long: FormControl;
  addr: FormControl;
  uploadfilename: FormControl;

  constructor(
    private widgets: WidgetService,
    private sanitizer: DomSanitizer,
    private platform: Platform,
    private nf: NativeFunction
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    if ((this.platform.is('mobile') && this.platform.is('hybrid')) || this.platform.is('desktop')) {
      this.isDesktop = true;
    }
    console.log(this.platform.platforms());
  }

  
  async reset() {
    this.photo = null;
    this.myform.reset();
  }

  createFormControls() {
    this.keterangan = new FormControl("", Validators.required);
    this.lat = new FormControl("", Validators.required);
    this.long = new FormControl("", Validators.required);
    this.addr = new FormControl("", Validators.required);
    this.uploadfilename = new FormControl("", Validators.required);
  }

  createForm() {
    this.myform = new FormGroup({
      keterangan: this.keterangan,
      lat: this.lat,
      long: this.long,
      addr: this.addr,
      uploadfilename: this.uploadfilename
    });
  }

  async getLocation() {
    try {
      const loading = await this.widgets.loading();
      loading.present();
      const coordinates = await this.nf.getLocation();
      this.latitude = coordinates.coords.latitude;
      this.longitude = coordinates.coords.longitude;
      console.log(coordinates);
      this.success(loading);
      this.addr.setValue("Kuala")
    } catch (error) {
      console.log("GPS ERROR");
    }
  }

  async takePicture(type: string) {
    console.log(this.isDesktop && type === 'gallery');
    if (type === 'gallery') {
      this.filePickerRef.nativeElement.click();
      return;
    }
    try {
      const reader = new FileReader();
      const image = await Camera.getPhoto({
        quality: 100,
        width: 400,
        allowEditing: false,
        resultType: CameraResultType.Uri,
        source: CameraSource.Prompt
      });
      const photoInTempStorage = await Filesystem.readFile({ path: image.path });
      console.log(photoInTempStorage);  

      let date = new Date(),
        time = date.getTime(),
        fileName = time + ".jpeg";
      
      await Filesystem.writeFile({
        data: photoInTempStorage.data,
        path: fileName,
        directory: FilesystemDirectory.Data
      });

      const finalPhotoUri = await Filesystem.getUri({
        directory: FilesystemDirectory.Data,
        path: fileName
      });

      this.photo = Capacitor.convertFileSrc(finalPhotoUri.uri);
      console.log(this.photo);  
    }
    catch (err)
    {
      const loading = await this.widgets.loading();
      loading.present();
      this.success(loading);
    }
    // var imageUrl = image.webPath;
    // this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl))
    // this.uploadfilename.setValue(image.dataUrl);
  };

  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      console.log('File format not supported');
      return;
    }
    reader.onload = () => {
      this.photo = reader.result.toString();
      this.uploadfilename.setValue(reader.result.toString());
    };
    reader.readAsDataURL(file);
  }

  success(loading) {
    loading.dismiss();
    this.widgets.showSuccessModal();
  }

  // async takePicture(type: string) {
  //   console.log(this.isDesktop && type === 'gallery');
  //   if (type === 'gallery') {
  //     this.filePickerRef.nativeElement.click();
  //     return;
  //   }

  //   const reader = new FileReader();
  //   const image = await Camera.getPhoto({
  //     quality: 100,
  //     width: 400,
  //     allowEditing: false,
  //     resultType: CameraResultType.DataUrl,
  //     source: CameraSource.Prompt
  //   });
  //   this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl))
  //   this.uploadfilename.setValue(image.dataUrl);
  // };

  

  remainingWords() {
    try {
      const l = 700 - (this.myform ? this.myform.value.keterangan.length : 0)
      // if (l < 0) {
      //   //TODO: limit text
      // }
      return l > 0 ? l : 0;      
    } catch (error) {
      return 500;
    }
  }

}
