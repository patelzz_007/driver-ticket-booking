import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SosPageRoutingModule } from './sos-routing.module';

import { SosPage } from './sos.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SosPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule,
    ReactiveFormsModule
  ],
  declarations: [SosPage]
})
export class SosPageModule { }
