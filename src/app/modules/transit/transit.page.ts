import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-transit',
  templateUrl: './transit.page.html',
  styleUrls: ['./transit.page.scss'],
})
export class TransitPage implements OnInit {

  isHidden: boolean = true;
  page: string = "";
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.subscribeToRouter();
  }

  subscribeToRouter() {
    this.router.events.subscribe((val: NavigationEnd) => {
        if(val.url){
          this.page = val.url;
          this.isHidden = this.isHiddenL(this.page);
        }
    });
  }

  isHiddenL(page){
    const pages = [
      "/transit",
      "/transit/sos",
      "/transit/alert",
      "/transit/complaints",
    ];

    return !pages.includes(page);
  }



  checkActive(page){
    return this.isPage(page) ? 'active-tab' : '';
  }

  isPage(page){
    return this.page.includes(page) ?? false;
  }

}
