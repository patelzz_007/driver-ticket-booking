import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransitPageRoutingModule } from './transit-routing.module';

import { TransitPage } from './transit.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransitPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule,
    ReactiveFormsModule
  ],
  declarations: [TransitPage]
})
export class TransitPageModule {}
