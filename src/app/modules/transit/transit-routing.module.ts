import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransitPage } from './transit.page';

const routes: Routes = [
  {
    path: '',
    component: TransitPage,
    children:[
      {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'alert',
        loadChildren: () => import('./pages/alert/alert.module').then( m => m.AlertPageModule)
      },
      {
        path: 'sos',
        loadChildren: () => import('./pages/sos/sos.module').then( m => m.SosPageModule)
      },
      {
        path: 'complaints',
        loadChildren: () => import('./pages/complaints/complaints.module').then( m => m.ComplaintsPageModule)
      },
    ]
  },
  
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransitPageRoutingModule {}
