import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VehicleInfoPageRoutingModule } from './vehicle-info-routing.module';

import { VehicleInfoPage } from './vehicle-info.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VehicleInfoPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule  
  ],
  declarations: [VehicleInfoPage]
})
export class VehicleInfoPageModule {}
