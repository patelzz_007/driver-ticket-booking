import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShortlinkPageRoutingModule } from './shortlink-routing.module';

import { ShortlinkPage } from './shortlink.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShortlinkPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [ShortlinkPage]
})
export class ShortlinkPageModule {}
