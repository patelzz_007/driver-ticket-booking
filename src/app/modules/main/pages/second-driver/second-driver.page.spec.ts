import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SecondDriverPage } from './second-driver.page';

describe('SecondDriverPage', () => {
  let component: SecondDriverPage;
  let fixture: ComponentFixture<SecondDriverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondDriverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SecondDriverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
