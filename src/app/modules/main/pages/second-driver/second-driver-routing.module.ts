import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecondDriverPage } from './second-driver.page';

const routes: Routes = [
  {
    path: '',
    component: SecondDriverPage,
    children:[
      {
        path: '',
        loadChildren: () => import('./pages/main/main.module').then( m => m.MainPageModule)
      },
      {
        path: 'swap-drivers',
        loadChildren: () => import('./pages/swap-drivers/swap-drivers.module').then( m => m.SwapDriversPageModule)
      },
      {
        path: 'accept-swap',
        loadChildren: () => import('./pages/accept-swap/accept-swap.module').then( m => m.AcceptSwapPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SecondDriverPageRoutingModule {}
