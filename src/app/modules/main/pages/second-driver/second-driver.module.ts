import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SecondDriverPageRoutingModule } from './second-driver-routing.module';

import { SecondDriverPage } from './second-driver.page';


import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SecondDriverPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [SecondDriverPage]
})
export class SecondDriverPageModule {}
