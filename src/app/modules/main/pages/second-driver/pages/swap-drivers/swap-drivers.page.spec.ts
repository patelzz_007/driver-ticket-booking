import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SwapDriversPage } from './swap-drivers.page';

describe('SwapDriversPage', () => {
  let component: SwapDriversPage;
  let fixture: ComponentFixture<SwapDriversPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapDriversPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SwapDriversPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
