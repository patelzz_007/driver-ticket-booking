import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Plugins, CameraResultType, CameraSource, Geolocation, Capacitor } from '@capacitor/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';

import { WidgetService } from 'src/app/services/common';

const { Camera } = Plugins;

@Component({
  selector: 'app-swap-drivers',
  templateUrl: './swap-drivers.page.html',
  styleUrls: ['./swap-drivers.page.scss'],
})
export class SwapDriversPage implements OnInit {

  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  latitude: number;
  longitude: number;
  isDesktop: boolean;
  photo: SafeResourceUrl;
  uploadfilename: any;
  file: string;
  address: string;

  constructor(
    private widgets: WidgetService,
    private sanitizer: DomSanitizer,
    private platform: Platform,
    // private nativeGeocoder: NativeGeocoder,
  ) { }

  ngOnInit() {
  }

  success(loading) {
    loading.dismiss();
    this.widgets.showSuccessModal();
  }


  async takePicture(type: string) {
    if (!Capacitor.isPluginAvailable('Camera') || (this.isDesktop && type === 'gallery')) {
      this.filePickerRef.nativeElement.click();
      return;
    }

    const image = await Camera.getPhoto({
      quality: 100,
      width: 400,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Prompt
    });

    this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl))
    this.file = image.dataUrl;
    // this.form.uploadfilename = this.file;
    // console.log(this.form.uploadfilename);
  }

  async getLocation() {
    const loading = await this.widgets.loading();
    loading.present();
    const coordinates = await Geolocation.getCurrentPosition();
    this.latitude = coordinates.coords.latitude;
    this.longitude = coordinates.coords.longitude;
    this.success(loading);

    // console.log('current', coordinates);

    // const apiz = await this.aduanApi.adress(this.latitude, this.longitude);
    // console.log(apiz);
  }

}
