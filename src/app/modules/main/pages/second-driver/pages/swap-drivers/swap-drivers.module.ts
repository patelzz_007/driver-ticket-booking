import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SwapDriversPageRoutingModule } from './swap-drivers-routing.module';

import { SwapDriversPage } from './swap-drivers.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SwapDriversPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [SwapDriversPage]
})
export class SwapDriversPageModule {}
