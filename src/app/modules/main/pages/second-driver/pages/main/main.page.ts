import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  constructor(
    private nav : NavController
  ) { }

  ngOnInit() {
  }

  swapDrivers(){
    this.nav.navigateForward('/main/second-driver/swap-drivers')

  }
}
