import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcceptSwapPage } from './accept-swap.page';

describe('AcceptSwapPage', () => {
  let component: AcceptSwapPage;
  let fixture: ComponentFixture<AcceptSwapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptSwapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcceptSwapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
