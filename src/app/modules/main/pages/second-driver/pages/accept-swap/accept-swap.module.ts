import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcceptSwapPageRoutingModule } from './accept-swap-routing.module';

import { AcceptSwapPage } from './accept-swap.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcceptSwapPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [AcceptSwapPage]
})
export class AcceptSwapPageModule {}
