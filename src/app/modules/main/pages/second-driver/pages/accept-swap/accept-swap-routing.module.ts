import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcceptSwapPage } from './accept-swap.page';

const routes: Routes = [
  {
    path: '',
    component: AcceptSwapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcceptSwapPageRoutingModule {}
