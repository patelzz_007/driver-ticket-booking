import { Component, OnInit, ViewChild } from '@angular/core';

import { MenuController, IonSlides } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { AppCommon } from 'src/app/services/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  sliderTwo: any;

  slideOptsTwo = {
    initialSlide: 1,
    slidesPerView: 2,
    loop: true,
    centeredSlides: true,
    spaceBetween: 20
  };

  favs: [] = [];
  recents = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuController,
    private app: AppCommon,
    ) {
      this.sliderTwo =
      {
        isBeginningSlide: true,
        isEndSlide: false
      };
    }

    ionViewWillEnter() {
      this.setFav();
      this.setRecent();
    }
  
  
  ngOnInit() {

  }


  async setFav() {
    this.favs = await this.app.getFavourite();
  }

  async setRecent() {
    this.recents = await this.app.getRecent();
    
  }

  //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }

}
