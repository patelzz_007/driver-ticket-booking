import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TravelInfoPageRoutingModule } from './travel-info-routing.module';

import { TravelInfoPage } from './travel-info.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TravelInfoPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [TravelInfoPage]
})
export class TravelInfoPageModule {}
