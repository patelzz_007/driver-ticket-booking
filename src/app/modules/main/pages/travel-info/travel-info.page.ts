import { Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides } from '@ionic/angular';  

@Component({
  selector: 'app-travel-info',
  templateUrl: './travel-info.page.html',
  styleUrls: ['./travel-info.page.scss'],
})
export class TravelInfoPage implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;  
  segment = 0;  
  type: string;

  constructor() { }

  ngOnInit() {
  }

  async segmentChanged(ev: any) {  
    await this.slider.slideTo(this.segment);  
  }  
  async slideChanged() {  
    this.segment = await this.slider.getActiveIndex();  
  }  

}
