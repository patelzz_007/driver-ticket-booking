import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPage } from './main.page';

export const routes: Routes = [
  {
    path: '',
    component: MainPage,
    children: [

      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'main-profil',
        loadChildren: () => import('./pages/profil/profil.module').then(m => m.ProfilPageModule)
      },
      {
        path: 'shortlink',
        loadChildren: () => import('./pages/shortlink/shortlink.module').then(m => m.ShortlinkPageModule)
      }, 
      {
        path: 'second-driver',
        loadChildren: () => import('./pages/second-driver/second-driver.module').then(m => m.SecondDriverPageModule)
      },
      {
        path: 'travel-info',
        loadChildren: () => import('./pages/travel-info/travel-info.module').then(m => m.TravelInfoPageModule)
      },
      {
        path: 'vehicle-info',
        loadChildren: () => import('./pages/vehicle-info/vehicle-info.module').then(m => m.VehicleInfoPageModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule { }
