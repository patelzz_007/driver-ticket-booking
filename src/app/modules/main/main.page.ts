import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  isHidden: boolean = true;
  page: string = "";
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.subscribeToRouter();
  }

  subscribeToRouter() {
    this.router.events.subscribe((val: NavigationEnd) => {
        if(val.url){
          this.page = val.url;
          this.isHidden = this.isHiddenL(this.page);
        }
    });
  }

  isHiddenL(page){
    const pages = [
      "/main/home",
      "/main/main-profil",
      "/main/shortlink",
      "/main/profile",
      "/main/second-driver",
      "/main/transaction-history",
      "/main/travel-info",
      "/main/vehicle-info",
      "/transit",
      "/transit/sos",
      "/transit/alert",
      "/transit/complaints",
    ];

    return !pages.includes(page);
  }



  checkActive(page){
    return this.isPage(page) ? 'active-tab' : '';
  }

  isPage(page){
    return this.page.includes(page) ?? false;
  }

}