import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then( m => m.AuthModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainPageModule),
  },
  
  {
    path: 'transit',
    loadChildren: () => import('./modules/transit/transit.module').then( m => m.TransitPageModule)
  },

  {
    path: 'check-in',
    loadChildren: () => import('./modules/check-in/check-in.module').then( m => m.CheckInPageModule)
  },

  {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full'
  },
  {
    path: 'check-in',
    loadChildren: () => import('./modules/check-in/check-in.module').then( m => m.CheckInPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
