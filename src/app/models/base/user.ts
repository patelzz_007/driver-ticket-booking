export interface User {
    fname: string,
    lname: string,
    email: string,
    address: Array<Address>,
    billing: Array<any>,
    uid: string,
    did: string,
    aid: string
  }
  
  export interface Address {
    first_name: string,
    last_name: string,
    address_line_1: string,
    address_line_2: string,
    country: string,
    state: string,
    city: string,
    zipcode: number,
    phone_number: number
  }
  