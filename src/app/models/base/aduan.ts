export class Semakanaduan{
    public logid: number;
    public definasiuser: string;
    public ticketnum: string;
    public dtreceived: string;
    public module_code: string;
  }

  export class Aduandetail{
    public logid: number;
    public ticketnum: string;
    public module_user: string;
    public actiontaken: string;
    public probdesc: string;
    public attachmentID: string;
    public filename: string;
    public filepath: string;
  }

  export class Aduan {
    public keterangan: string;
    public lat: number;
    public long: number;
    public addr: string;
    public uploadfilename: string; 
  }
  
  export class Cadangan {
    public cadangan: string;
    public lat: number;
    public lng: number;
    public addr: string;
  }