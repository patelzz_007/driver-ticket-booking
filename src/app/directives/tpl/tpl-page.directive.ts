import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[tplPage]'
})
export class TemplatePageDirective {

  constructor(public tpl: TemplateRef<any>) { }

}