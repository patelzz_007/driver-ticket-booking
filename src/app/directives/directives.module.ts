import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TemplateFormDirective, TemplateMenuDirective, TemplateBodyDirective ,TemplatePageDirective} from './';

const directives = [
  TemplateMenuDirective,
  TemplateFormDirective,
  TemplatePageDirective,
  TemplateBodyDirective
];

@NgModule({
  declarations: directives,
  imports: [
    CommonModule,
    IonicModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports:directives,
})
export class AppDirectivesModule {
  static forRoot() {
      return {
          ngModule: AppDirectivesModule,
          providers: []
      }
  } 
}
