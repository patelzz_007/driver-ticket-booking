import { Injectable } from '@angular/core';
import { NativeInterface } from './native.interface';
import { Geolocation } from '@capacitor/core';

@Injectable({
    providedIn: 'root'
})

export class WebNative implements NativeInterface {
    constructor(
    ) {
    }

    camera(): number {
        return 1;
    }

    download(): number {
        return 1;
    }

    async getLocation() {
        try {
            if (navigator.geolocation) {
                return new Promise((resolve, reject) => {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        resolve(position);
                    });

                });
            } else {
                throw new Error("Geolocation is not supported by this browser.");
            }
        } catch (error) {
            throw new Error(error);
        }
    }

}
