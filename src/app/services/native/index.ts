import { Injectable } from '@angular/core';
import { NativeInterface } from './native.interface';
import { AndroidNative } from './android';
import { IosNative } from './ios';
import { WebNative } from './web';
import { Platform } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class NativeFunction implements NativeInterface {
    private nf: NativeInterface;
    constructor(
        private android: AndroidNative,
        private ios: IosNative,
        private web: WebNative,
        private platform: Platform
    ) {
    }

    init() {
        if ((this.platform.is('mobile') && this.platform.is('hybrid')) || this.platform.is('desktop')) {
            this.nf = this.web;
        }

        if (this.platform.is('android')) {
            this.nf = this.android;
        }

        if (this.platform.is('ios')) {
            this.nf = this.ios;
        }

        console.log(this.nf);
        
    }

    getLocation(): Promise<any> {
        return this.nf.getLocation();
    }

    camera(): number {
        return this.nf.camera();
    }

    download(): number {
        return this.nf.download();
    }


}
