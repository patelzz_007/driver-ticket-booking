export declare interface  NativeInterface {
    download(): any;
    camera(): any;
    getLocation(): Promise<any>;
}
