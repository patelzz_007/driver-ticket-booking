import { Injectable } from '@angular/core';
import { NativeInterface } from './native.interface';
import { Geolocation } from '@capacitor/core';

@Injectable({
    providedIn: 'root'
})

export class IosNative implements NativeInterface {
    constructor() {
    }
    
    camera(): number {
        return 1;
    }

    download(): number {
        return 1;
    }

    async getLocation() {
        try {
            const coordinates = await Geolocation.getCurrentPosition();
            return coordinates;
        } catch (error) {
            throw new Error(error);
        }
    }

}
