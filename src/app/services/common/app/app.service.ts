import { Injectable } from '@angular/core';
import { StorageService } from '../../storage/storage';
import { Router, Route } from '@angular/router';
// import { routes as rootRoute } from 'src/app/app-routing.module';
// import { routes as mainRoute } from 'src/app/modules/main/main-routing.module';

@Injectable({
  providedIn: 'root'
})
export class AppCommon {

  constructor(
    private storage: StorageService,
    private router: Router
  ) {
  }

  getAllModuleRoutes() {
    return [
      {
        name: "2nd Driver",
        path: "/main/second-driver",
        icon: "assets/svg/cuti.svg",
        children: []
      },
      {
        name: "Travel Info",
        path: "/main/travel-info",
        icon: "assets/svg/maklumat_profil.svg",
        children: []
      },
      {
        name: "Vehicle Status",
        path: "/main/vehicle-info",
        icon: "assets/svg/eGL.svg",
        children: []
      },
      {
        name: "Transit",
        path: "/transit",
        icon: "assets/svg/eGL.svg",
        children: []
      },
      {
        name: "Check In",
        path: "/check-in",
        icon: "assets/svg/eGL.svg",
        children: []
      },
    ]
  }

  async addToFavourite(path) {
    let fav = await this.storage.get(this.storage.FAVOURITE);
    if (!fav) {
      fav = await this.firstTimeFavorite();
    }

    fav.forEach(async (val, index) => {
      if (path == val.path) {
        fav[index]["fav"]++;
      }
    });

    await this.storage.set(this.storage.FAVOURITE, fav);
  }

  async firstTimeFavorite() {
    const modules = this.getAllModuleRoutes();

    modules.forEach((value, index) => {
      modules[index]["fav"] = 0;
    });

    await this.storage.set(this.storage.FAVOURITE, modules);
    return modules;
  }

  async addToRecent(path) {
    let recents = await this.storage.get(this.storage.RECENT);
    if (!recents) {
      recents = await this.firstTimeRecent();
    }

    let remove = false;
    recents.forEach((value, index) => {
      if (path == value.path && path != recents[recents.length - 1].path) {
        recents.push(recents[index]);
        remove = true;
      }
    });

    if (remove) {
      const all = JSON.parse(JSON.stringify(recents));
      const last = all.splice((all.length - 1), 1);
      recents = recents.splice(0, (recents.length - 1)).filter(function (obj) {
        return obj.path !== path;
      });
      recents.push(last[0]);
    }

    await this.storage.set(this.storage.RECENT, recents);
  }

  async firstTimeRecent() {
    const modules = this.getAllModuleRoutes();
    const recents = [];
    modules.forEach((value, indexModule) => {
      recents.push(modules[indexModule]);
      // if (value.hasOwnProperty("children")) {
      //   value.children.forEach((value, indexPage) => {
      //     recents.push(modules[indexModule].children[indexPage]);
      //   });
      // }
    });


    console.log(recents);
    await this.storage.set(this.storage.RECENT, recents);
    return recents;
  }

  async getFavourite() {
    let fav = await this.storage.get(this.storage.FAVOURITE);
    if (!fav) fav = await this.firstTimeFavorite();
    fav = fav.sort((a, b) => {
      if (a.fav < b.fav) return 1;
      if (b.fav < a.fav) return -1;

      return 0;
    });
    return fav.splice(0, 4);
  }

  async getRecent() {
    let recent = await this.storage.get(this.storage.RECENT);

    if (!recent) recent = await this.firstTimeRecent();

    if (recent.length > 10) {
      recent = recent.splice(Math.max(recent.length - 10, 0));
      this.storage.set(this.storage.RECENT, recent);
    }


    let revert = [];
    for (let i = (recent.length - 1); i > -1; i--) {
      if (!revert.includes(recent[i])) {
        revert.push(recent[i]);
        if (revert.length == 4) break;
      }
    }

    return revert;
  }
}
