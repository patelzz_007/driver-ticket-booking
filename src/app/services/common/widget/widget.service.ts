import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { LoadingModal, SuccessmodalPage } from 'src/app/components';

@Injectable({
  providedIn: 'root'
})
export class WidgetService {

  constructor(
    private modalCtrl: ModalController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private actionSheetController: ActionSheetController,
    private toastController: ToastController
  ) { }

  async presentAlert(title: string, content: string) {
		const alert = await this.alertController.create({
			header: title,
			message: content,
			buttons: ['OK']
		})

		await alert.present();
  }
  
  async loading(message = 'Loading...'){
    const loading = await this.loadingController.create({
      message: message
    });

    return loading;
  }

  async presentActionSheet(title, buttons) {
    const actionSheet = await this.actionSheetController.create({
      header: title,
      buttons: buttons
    });
    await actionSheet.present();
  }

  async presentToast(message, color = '', duration = 2000) {
    const toast = await this.toastController.create({
      message: `${message}`,
      duration: duration,
      color: color
    });
    await toast.present();
  }

  async presentAlertConfirm(message, ok, cancel = null) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: cancel
        }, {
          text: 'Confirm',
          handler: ok
        }
      ]
    });

    await alert.present();
  }


  async showSuccessModal(toPage = null, text = 'Success'){
    let modal = await this.modalCtrl.create({
      component: SuccessmodalPage,
      backdropDismiss: false,
      componentProps: { toPage, text }
    });
   return await modal.present();
  }

  async showLoadingModal(text = "Loading..."){
    let modal = await this.modalCtrl.create({
      component: LoadingModal,
      backdropDismiss: false,
      componentProps: {
        text: text
      }
    });
   return modal;
  }
}
