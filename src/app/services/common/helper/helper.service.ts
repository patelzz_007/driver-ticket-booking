import { Injectable } from '@angular/core';
import { WidgetService } from '../widget/widget.service';
import { NavController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
    private http: HttpClient,
  ) { }

  capitalizeString(str) {
    if (!str) return "";
    if (!str.length) return "";

    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  isJSON(qrString: string): boolean {
    if (
      /^[\],:{}\s]*$/.test(
        qrString
          .replace(/\\["\\\/bfnrtu]/g, "@")
          .replace(
            /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
            "]"
          )
          .replace(/(?:^|:|,)(?:\s*\[)+/g, "")
      )
    ) {
      return true;
    } else {
      return false;
    }
  }

  toObject(data) {
    const stringData = JSON.stringify(data);

    return JSON.parse(stringData);
  }

  generateName(): string {
    const name = new Date();
    return name.getTime().toString();
  }

  getFileType(filename) {
    return filename.split('.').pop();
  }

  extractImg(file) {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.onload = function (e) {
        try {
          resolve(e.target["result"].toString());
        } catch (err) {
          reject(err);
        }
      };
      reader.readAsDataURL(file);
    });
  }

  downloadImg(url) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function (event) {
        var blob = xhr.response;
        resolve(blob);
      };
      xhr.open('GET', url);
      xhr.send();
    });
  }
}
