
import { Component, OnInit, Input, ViewChild, ContentChild, TemplateRef } from '@angular/core';
import { TemplatePageDirective } from 'src/app/directives';
import { ActionSheetController, NavController, MenuController, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { WidgetService } from 'src/app/services/common';
import { ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-tpl-page',
  templateUrl: './tpl-page.page.html',
  styleUrls: ['./tpl-page.page.scss'],
})
export class TemplatePage implements OnInit {
  @ContentChild(TemplatePageDirective, { static: true, read: TemplateRef }) pageTpl: TemplateRef<any>;
  @Input() title: string;
  @Input() isBack: boolean;

  constructor(
    private actionSheetController: ActionSheetController,
    private router: Router,
    private nav: NavController,
    private widgets: WidgetService,
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuController,
  ) {  }

  ngOnInit() {
  }

  ionViewDidEnter() {
   
  }

  
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Pilihan',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Aduan',
          icon: 'list-outline',
          handler: this.navigate.bind(this, "/complaints/membuat-aduan")
        },
        {
          text: 'Semakan Status Aduan',
          icon: 'search-outline',
          handler: this.navigate.bind(this, "/complaints/semakan-aduan")
        },
        {
          text: 'Cadangan',
          icon: 'bulb-outline',
          handler: this.navigate.bind(this, "/complaints/cadangan")
        }, 
        
        {
          text: 'Mengenai',
          icon: 'information-outline',
          handler: this.navigate.bind(this, "/complaints/tentang-kami")
        },
        {
          text: 'Logout',
          icon: 'log-out-outline',
          handler: this.logout.bind(this)
        }
      ]
    });
    await actionSheet.present();
  }

  goBack() {
    this.nav.back();
  }


  logout(route){
    this.router.navigate([""]);
  }

  navigate(route){
    try{
      this.router.navigate([route]);
    } catch (e) {
      console.log(`Add "${route}" in routing`);
    }
  }
  
}
