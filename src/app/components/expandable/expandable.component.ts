import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-expandable',
  templateUrl: './expandable.component.html',
  styleUrls: ['./expandable.component.scss'],
})
export class ExpandableComponent implements OnInit {
  @Input() open: boolean;
  constructor() { }

  ngOnInit() {
    console.log(this.open);
  }

  showHideClass(){
    return this.open ? 'body-show' : 'body-hide';
  }

}
