import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal-loading',
  templateUrl: './modal-loading.component.html',
  styleUrls: ['./modal-loading.component.scss'],
})
export class LoadingModal implements OnInit {
  @Input() text: string;
  constructor(
  ) { }

  ngOnInit() {
  }

}
